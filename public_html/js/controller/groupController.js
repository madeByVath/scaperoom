(function () {
    angular.module('scapeRoomApp').controller('groupController', function ($scope, $window, finalObject) {

        // Controller properties
        // Scope variables
        $scope.user = new User();
        $scope.user.construct("userName", "userMail", "UserMovil");
        $scope.users = [];
        $scope.users.push($scope.user);
        $scope.group = new Group();
        $scope.group.construct("groupName", $scope.users, "WhatsApp", 5, "");
       
        //Modelo que permite agregar tareas
        $scope.addUser = function (userName, userMovil) {
            this.user = new User();
            this.user.construct(userName, "", userMovil);
            $scope.group.users.push(this.user);
        };

        // Modelo que permite eliminar tarea
        $scope.deleteUser = function (num, user) {
            if($scope.group.users.length>1){
                $scope.group.users.splice(num,1);
            }
        };
        
        $scope.setReservation = function () {
            //$scope.reservation.setEvent($scope.eventToInsert);
            //añadir objeto al padre
            //$scope.addReservation=finalObject;
            //alert($scope.addReservation);
            alert("Siguiente");
            $scope.$parent.showAction = 4;
        };

    }); // END Controller


   /**
    * @name: template add-canvas-view-form
    * @date: 27/01/2017
    * @version: 1
    * @author: Christian Vath
    * @description: show first view, form for create a canvas
    */
    angular.module('scapeRoomApp').directive("groupForm", function () {
        return {
            restrict: 'E', // type of directive
            templateUrl: "view/templates/group-form.html",
            controller: function () {
                // When the document is ready execute this code
            },
            controllerAs: 'groupForm' // This is the alias
        };
    });
    

})(); // END Angula function
