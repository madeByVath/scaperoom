(function () {
        angular.module('scapeRoomApp').controller('userController', function ($scope, $window, finalObject) {
            
        $scope.user = new User();
        $scope.user.construct("Max", "max@gmail.com", 654654654);
        
        $scope.setReservation = function () {
            //$scope.reservation.setEvent($scope.eventToInsert);
            //añadir objeto al padre
            //$scope.addReservation=finalObject;
            //alert($scope.addReservation);
            alert("Siguiente");
            $scope.$parent.showAction = 3;
        };

    }); // END Controller
    
    
    /**
    * @name: template add-canvas-view-form
    * @date: 27/01/2017
    * @version: 1
    * @author: Christian Vath
    * @description: show first view, form for create a canvas
    */
    angular.module('scapeRoomApp').directive("userForm", function () {
        return {
            restrict: 'E', // type of directive
            templateUrl: "view/templates/user-form.html",
            controller: function () {
            // When the document is ready execute this code
            },
            controllerAs: 'userForm' // This is the alias
        };
    });

})(); // END Angula function
