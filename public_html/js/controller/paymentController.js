// jQuery code
$(document).ready(function () {

});

//Angular code
(function () {
    angular.module('scapeRoomApp').controller('paymentController', function ($scope, $window, finalObject) {
        $scope.payment = new payDataObject();

    });

    angular.module('scapeRoomApp').directive("paymentForm", function () {
        return {
            restrict: 'E',
            templateUrl: "view/templates/payment-form.html",
            controller: function () {

            },
            controllerAs: 'paymentForm'
        };
    });
})();
