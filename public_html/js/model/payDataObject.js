function payDataObject(){
    //Attributes declaration
    this.creditCard;
    this.cvv;
    this.postalCode;

    this.construct = function (creditCard, cvv, postalCode) {
        this.creditCard = creditCard;
        this.cvv = cvv;
        this.postalCode = postalCode;
    };
    
    //Methods declaration
    this.setCreditCard = function (creditCard) {
        this.creditCard = creditCard;
    };
    this.setCvv = function (cvv) {
        this.cvv = cvv;
    };
    this.setPostalCode = function (postalCode) {
        this.postalCode = postalCode;
    };
    this.getCreditCard = function () {
        return this.creditCard;
    };
    this.getCvv = function () {
        return this.cvv;
    };
    this.getPostalCode = function () {
        return this.postalCode;
    };
}
