function User() {
    // Properties definition
    this.name;
    this.email;
    this.movil;

    // Methods definition
    this.construct = function (name, email, movil) {
        this.name = name;
        this.email = email;
        this.movil = movil;
    };

    // getter and setter
    this.getName = function () { return this.name; };
    this.setName = function (name) { this.name = name; };

    this.getMail = function () { return this.email; };
    this.setMail = function (email) { this.email = email; };

    this.getMovil = function () { return this.movil; };
    this.setMovil = function (movil) { this.movil = movil; };

} // END Canvas class
